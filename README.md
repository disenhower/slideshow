# Slideshow Project

Website Template Viewer application including front-end SPA based on ReactJS and a back-end service layer running ExpressJS using a file-based JSON datastore.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Requirements

* Node v14.15.4+

## Setup

Run `npm install` in the project directory to download dependencies and other required packages.
## Available Scripts

In the project directory, you can run:

### `npm start`

Starts Api server at [http://localhost:4000](http://localhost:4000) and runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view the application in the browser.


Prior to execution, setting a "DATAFILE" environment variable with a path to a JSON file will use the file as the JSON datastore for the api server.  Otherwise, the default datastore under './data/templates.json' is used.


Example
```
DATAFILE='./data/extendedTemplate.json' npm run start
```


The page will reload if you make edits.\
You will also see any lint errors in the console.

<!-- ### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information. -->

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run start-api`

Runs only the API server application at [http://localhost:4000](http://localhost:4000)

Prior to execution, setting a "DATAFILE" environment variable with a path to a JSON file will use the file as the JSON datastore for the api server.  Otherwise, the default datastore under './data/templates.json' is used.

Example
```
DATAFILE='./data/extendedTemplate.json' npm run start-api
```

<!-- ### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it. -->

