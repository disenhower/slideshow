var DATAFILE = process.env.DATAFILE || "./data/templates.json"

const data = require(DATAFILE)
console.log("Serving data from file: " + DATAFILE);
var cors = require('cors');

const express = require('express');
const app = express();
const port = 4000;

app.use(cors());

app.get('/', (req, res) => {
    res.contentType('json').send(data);
})

app.listen(port, () => {
  console.log(`WebTemplate API App listening at http://localhost:${port}`)
})