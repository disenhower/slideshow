import React, { Component } from 'react'
import PropTypes from 'prop-types'

export class WebTemplateThumbnail extends Component {

    handleClick = (e) => {
        this.props.selectedTemplateChangeListener(this.props.template.id);
    }

    render() {
        const { id, thumbnail } = this.props.template;
        let thumnailPath = "images/thumbnails/" + thumbnail;
        let alt = id + "-m";
        let href = "#" + id;
        let cssClass = this.props.selectedTemplateId === id ? "active" : "";
        return (
            <a onClick={this.handleClick} href={href} title={id} className={cssClass}>
                <img src={thumnailPath} alt={alt} width="145" height="121" />
                <span>{id}</span>
            </a>
        )
    }
}

WebTemplateThumbnail.propTypes = {
    template: PropTypes.object.isRequired,
    selectedTemplateChangeListener: PropTypes.object
};

export default WebTemplateThumbnail;
