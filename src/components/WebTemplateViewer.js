import React, { Component } from 'react'
import PropTypes from 'prop-types';
import WebTemplate from './WebTemplate'
import ThumbnailViewer from './ThumbnailViewer'

class WebTemplateViewer extends Component {

  render() {
    if (this.props.templates === null || this.props.templates.length === 0) {
      return (<div><h1>No Templates Found</h1><h2>Server not available or no templates found</h2></div>);
    }
    
    let selectedTemplate = this.props.templates.find( (template) => (template.id === this.props.selectedTemplateId) );
    return (
      <div id="main" role="main">
        <WebTemplate template={selectedTemplate}/>
        <ThumbnailViewer 
          templates={this.props.templates} 
          selectedTemplateId={this.props.selectedTemplateId} 
          selectedTemplateChangeListener={this.props.selectedTemplateChangeListener}/>
      </div>
    );
  }

}

WebTemplateViewer.propTypes = {
  templates: PropTypes.array.isRequired,
  selectedTemplateId: PropTypes.string,
  selectedTemplateChangeListener: PropTypes.object
};


export default WebTemplateViewer;
