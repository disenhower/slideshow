import React, { Component } from 'react'
import PropTypes from 'prop-types'

export class WebTemplate extends Component {
    render() {
		let tmplt = this.props.template;
		let imgPath = "images/large/" + tmplt.image;
        return (
			<div id="large">
				<div class="group">
					<img src={imgPath} alt="Large" width="430" height="360" />
					<div class="details">
						<p><strong>Title</strong> {tmplt.title}</p>
						<p><strong>Description</strong> {tmplt.description}</p>
						<p><strong>Cost</strong> ${tmplt.cost}</p>
						<p><strong>ID #</strong> {tmplt.id}</p>
						<p><strong>Thumbnail File</strong> {tmplt.thumbnail}</p>
						<p><strong>Large Image File</strong> {tmplt.image}</p>
					</div>
				</div>
			</div>
        )

    }

}

WebTemplate.propTypes = {
	template: PropTypes.object.isRequired
};
  
export default WebTemplate
