import React, { Component } from 'react'
import PropTypes from 'prop-types';
import WebTemplateThumbnail from './WebTemplateThumbnail'

class ThumbnailViewer extends Component {

    thumbnailSpan = 4;
    indexOfSelected = 0;

    getSpanStartIndex = (selectedIndex) => {
        return selectedIndex - (selectedIndex % this.thumbnailSpan);
    } 

    getSpanNextIndex = (selectedIndex) => {
        return this.getSpanStartIndex(selectedIndex) + this.thumbnailSpan;
    } 

    next = () => {
        let templateId = this.props.templates[this.getSpanNextIndex(this.indexOfSelected)].id;
        this.props.selectedTemplateChangeListener(templateId);
    }

    previous = () => {
        let templateId = this.props.templates[this.getSpanStartIndex(this.indexOfSelected)-1].id;
        console.debug("going prev = " + templateId)
        this.props.selectedTemplateChangeListener(templateId);
    }

    render() {
        let templates = this.props.templates;
        this.indexOfSelected = templates.findIndex(template => (template.id === this.props.selectedTemplateId));

        let startIndex = this.getSpanStartIndex(this.indexOfSelected);
        let nextIndex = this.getSpanNextIndex(this.indexOfSelected);
        let prevDisabled = (startIndex <= 0);
        let nextDisabled = (nextIndex >= templates.length);

        return (
            <div className="thumbnails">
                <div className="group">
                    {
                        templates.filter( (template, index) => (index >= startIndex && index < nextIndex) )
                            .map( (template) => {
                            return ( 
                                <WebTemplateThumbnail 
                                    template={template} 
                                    selectedTemplateId={this.props.selectedTemplateId} 
                                    selectedTemplateChangeListener={this.props.selectedTemplateChangeListener} />
                            )
                        })
                    }
                    <span hidden={!prevDisabled} className="previous disabled" title="Previous">Previous</span>
                    <a hidden={prevDisabled} onClick={this.previous} href="#prev" className="previous" title="Previous">Previous</a>
                    <span hidden={!nextDisabled} className="next disabled" title="Next">Next</span>
                    <a hidden={nextDisabled} onClick={this.next} href="#next" className="next" disabled={nextDisabled} title="Next">Next</a>
                </div>
            </div>
        )
    }
}

ThumbnailViewer.propTypes = {
    templates: PropTypes.array.isRequired,
    selectedTemplateId: PropTypes.number,
    selectedTemplateChangeListener: PropTypes.object
};

export default ThumbnailViewer
