import React, { Component } from 'react'
import WebTemplateViewer from './components/WebTemplateViewer'

class App extends Component {

  state = {
    templates: []
  }

  selectedTemplateChangeListener = (templateId) => {
    this.setState({selectedTemplateId: templateId});
  }

  constructor(props) {
    super(props);
    fetch("http://localhost:4000/")
      .then(res => res.json())
      .then(
        (templates) => {
          if(templates) {
            this.setState({
              isLoaded: true,
              templates: templates,
              selectedTemplateId: templates[0].id
            });
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    return (
      <div className="App">
        <WebTemplateViewer templates={this.state.templates} selectedTemplateId={this.state.selectedTemplateId} selectedTemplateChangeListener={this.selectedTemplateChangeListener}/>
      </div>
    );
  }

}

export default App;
